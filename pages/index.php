<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raffle System</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/dist/css/sweetalert2.min.css" rel="stylesheet" >
    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      #output {
          margin: 20px;
          padding: 20px;
          background: gray;
          border-radius: 10px;
          font-size: 80px;
          width: 200px;
          color: white;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <script src="../assets/dist/js/sweetalert2.all.min.js"></script>
    <script src="../assets/dist/js/sweetalert2.min.js"></script>
    <script src="../assets/dist/js/jquery.min.js"></script>
    <script type="text/javascript" src="../assets/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Custom styles for this template -->
    <link href="../assets/dist/css/cover.css" rel="stylesheet">
  </head>
  <body class="d-flex h-100 text-center text-white bg-dark">
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="mb-auto">
        <div>
          <h3 class="float-md-start mb-0">Raffle System</h3>
          <nav class="nav nav-masthead justify-content-center float-md-end">
            <a class="nav-link active" aria-current="page" href="#">Home</a>
            <a class="nav-link" href="features.php">Winners</a>
            <a class="nav-link" href="#">Contact</a>
          </nav>
        </div>
      </header>

      <main class="px-3">
        <h1>Raffle System.</h1>
        <p class="lead">Press the button to start..</p>
        <p class="lead">
          <button type="button" class="btn btn-lg btn-secondary fw-bold border-white bg-white" data-bs-toggle="modal" data-bs-target="#raffleDrawModal">
            Start
          </button>
        </p>
      </main>

      <footer class="mt-auto text-white-50">
        <p><a href="" class="text-white">Raffle System</a>, by <a href="" class="text-white">Dominic Tucay</a>.</p>
      </footer>
    </div>
  </body>

  <!-- Modal -->
  <div class="modal fade text-black bg-dark" id="raffleDrawModal" tabindex="-1" aria-labelledby="raffleDrawModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content rounded-6 shadow">
        <div class="modal-header border-bottom-0">
          <h5 id="raffleDrawModal_title" class="modal-title">Generating Winning Number....</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body py-0">
          <div class="d-flex align-items-center justify-content-center">
            <div id="output">
              --
            </div>
            <div>
              <dl id="winnerDetails" class="row" hidden="true">
                <dt class="col-sm-4">Ticket #</dt>
                <dd class="col-sm-8"><p id="ticketno"></p></dd>

                <dt class="col-sm-4">Name</dt>
                <dd class="col-sm-8">
                  <p id="name"></p>
                </dd>

                <!--<dt class="col-sm-4">Email</dt>
                <dd class="col-sm-8">
                  <p id="email"></p>
                </dd>
                -->
              </dl>
            </div>  
          </div>
        </div>
        <div class="modal-footer flex-column border-top-0">
        </div>
      </div>
    </div>
  </div>
</html>

<script>
   $( document ).ready(function() {

    
    var output, duration, desired;

    // Initial setup
    output = $('#output');
    
    $('#raffleDrawModal').on('shown.bs.modal', function (e) {
        $.ajax({
          type:"POST",
          url: "php/generate_winning_ticket.php",
          dataType: 'json',
          success: function(res){
            if(!res.limit){
              desired = res.ticket_number;
              $('#ticketno').html(res.ticket_number);
              $('#name').html(res.full_name);
              //$('#email').html(res.email);
              console.log(desired)
              var ranking;

              switch(res.rank) {
                case 5:
                  ranking = '5th';
                  break;
                case 4:
                  ranking = '4th';
                  break;
                case 3:
                  ranking = '3rd';
                  break;
                case 2:
                  ranking = '2nd';
                  break;
                case 1:
                  ranking = '1st';
                  break;
                }


              $('#raffleDrawModal_title').text(ranking + ' Winning Number....')


               // Animate!
                animationTimer = setInterval(function() {
                    // If the value is what we want, stop animating
                    // or if the duration has been exceeded, stop animating
                    if (output.text().trim() === desired) {
                        clearInterval(animationTimer);
                        $('#raffleDrawModal_title').text('Congratulations!!!!!')
                        $('#winnerDetails').removeAttr('hidden');
                    } else {
                        output.text(
                            ''+
                            Math.floor(Math.random() * 10)+
                            Math.floor(Math.random() * 10)
                        );
                    }
                }, 100);
            }
            else{
              $('#raffleDrawModal').modal('hide');
              Swal.fire({
                icon: 'info',
                title: 'Check Winners Tab',
                text: 'Already generated 5 winning tickets. ',
              })
            }
          }
        });
    })

    $('#raffleDrawModal').on('hidden.bs.modal', function (e) {
      output.text('--');
      $('#winnerDetails').attr('hidden','true');
      $('#raffleDrawModal_title').text('Generating Winning Number....')
    })

  });
</script>
