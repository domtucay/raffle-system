<!doctype html>
<html lang="en" class="h-100">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Raffle System</title>

    <!-- Bootstrap core CSS -->
    <link href="../assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <script src="../assets/dist/js/jquery.min.js"></script>
    
    <!-- Custom styles for this template -->
    <link href="../assets/dist/css/cover.css" rel="stylesheet">
  </head>
  <body class="d-flex h-100 text-white bg-dark">
    
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
      <header class="mb-auto text-center">
        <div>
          <h3 class="float-md-start mb-0">Raffle System</h3>
          <nav class="nav nav-masthead justify-content-center float-md-end">
            <a class="nav-link" href="index.php">Home</a>
            <a class="nav-link active" aria-current="page" href="#">Winners</a>
            <a class="nav-link" href="#">Contact</a>
          </nav>
        </div>
      </header>

      <main class="px-3">
        <h1 class="text-center">Raffle System Winners</h1>
        <ul class="list-group list-group-flush">
          <li class="list-group-item list-group-item-dark d-flex justify-content-between align-items-center">
            <span class="badge bg-success rounded-pill">1st</span>
            <p id="rank1"></p>
          </li>
          <li class="list-group-item list-group-item-dark d-flex justify-content-between align-items-center">
           <span class="badge bg-primary rounded-pill">2nd</span>
           <p id="rank2"></p>
          </li>
          <li class="list-group-item list-group-item-dark d-flex justify-content-between align-items-center">
           <span class="badge bg-warning rounded-pill">3rd</span>
           <p id="rank3"></p>
          </li>
          <li class="list-group-item list-group-item-dark d-flex justify-content-between align-items-center">
            <span class="badge bg-info rounded-pill">4th</span>
            <p id="rank4"></p>
          </li>
          <li class="list-group-item list-group-item-dark d-flex justify-content-between align-items-center">
           <span class="badge bg-secondary rounded-pill">5th</span>
           <p id="rank5"></p>
          </li>
        </ul> 
        
      </main>

      <footer class="mt-auto text-white-50 text-center">
        <p><a href="" class="text-white">Raffle System</a>, by <a href="" class="text-white">Dominic Tucay</a>.</p>
      </footer>
    </div>
    
  </body>
</html>


<script>
  $( document ).ready(function() {
    
        $.ajax({
          type:"POST",
          url: "php/get_winners.php",
          dataType: 'json',
          success: function(res){
            $.each(res, function(i,data){
              switch(data.rank) {
                case '1':
                  $('#rank1').html(data.full_name);
                  break;
                case '2':
                  $('#rank2').html(data.full_name);
                  break;
                case '3':
                  $('#rank3').html(data.full_name);
                  break;
                case '4':
                  $('#rank4').html(data.full_name);
                  break;
                case '5':
                  $('#rank5').html(data.full_name);
                  break;
                }
            })
           
          }
        });
  });
</script>
